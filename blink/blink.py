import RPi.GPIO as GPIO # Import the GPIO library which lets us control the GPIO pins in Python
import time

GPIO.setmode(GPIO.BCM) # This tells the GPIO library to use board numbering.
GPIO.setwarnings(False) 

GPIO.setup(4, GPIO.OUT) # Setup GPIO 4 as an output

while True: # This is an infinite loop which will keep turning the led on and off until the program is stopped
    GPIO.output(4, True) # Makes GPIO 4 HIGH which means the led has power.
    time.sleep(0.8) # Adds a delay so the led will stay on for 0.8
    GPIO.output(4, False) # Makes GPIO 4 LOW which means the led has no power.
    time.sleep(0.8) # Adds a delay so the led will stay off for 0.8 seconds